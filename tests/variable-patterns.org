;;; Gash -- Guile As SHell
;;; Copyright © 2016, 2017, 2018 R.E.W. van Beusekom <rutger.van.beusekom@gmail.com>
;;; Copyright © 2018, 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

* hash-hash
:script:
#+begin_src sh
  file=dir/sub/name.ext
  echo ${file##*/}
#+end_src
:stdout:
#+begin_example
  name.ext
#+end_example

* hash
:script:
#+begin_src sh
  file=dir/sub/name.ext
  echo ${file#*/}
#+end_src
:stdout:
#+begin_example
  sub/name.ext
#+end_example

* percent-percent
:script:
#+begin_src sh
  file=dir/sub/name.ext
  echo ${file%%/*}
#+end_src
:stdout:
#+begin_example
  dir
#+end_example

* percent
:script:
#+begin_src sh
  file=dir/sub/name.ext
  echo ${file%/*}
#+end_src
:stdout:
#+begin_example
  dir/sub
#+end_example

* percent-space
:script:
#+begin_src sh
  args="--prefix=/usr "
  echo ${args% *}/
#+end_src
:stdout:
#+begin_example
  --prefix=/usr/
#+end_example

;;; Gash -- Guile As SHell
;;; Copyright © 2016, 2017, 2018 R.E.W. van Beusekom <rutger.van.beusekom@gmail.com>
;;; Copyright © 2018, 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

* and
:script:
#+begin_src sh
  true && exit 2
#+end_src
:status: 2

* exec
:script:
#+begin_src sh
  exec true
  false
#+end_src

* or
:script:
#+begin_src sh
  false || true
#+end_src

* pipe-exit-0
:script:
#+begin_src sh
  false | true
#+end_src

* pipe-exit-1
:script:
#+begin_src sh
  true | false
#+end_src
:status: 1

* pipe-sed-cat
:script:
#+begin_src sh
  echo -e 'a\nb\nc' tests/data/star/* | sed 's, ,\n,g' | cat
#+end_src
:stdout:
#+begin_example
  a
  b
  c
  tests/data/star/0
  tests/data/star/1
  tests/data/star/2
  tests/data/star/3
#+end_example

* pipe-sed
:script:
#+begin_src sh
  echo -e 'a\nb\nc' tests/data/star/* | \sed 's, ,\n,g'
#+end_src
:stdout:
#+begin_example
  a
  b
  c
  tests/data/star/0
  tests/data/star/1
  tests/data/star/2
  tests/data/star/3
#+end_example

* semi
:script:
#+begin_src sh
  # gash makes this into a pipeline, then uses `true''s exit status
  false ; true
#+end_src
:status: 1

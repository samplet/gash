;;; Gash -- Guile As SHell
;;; Copyright © 2016, 2017, 2018 R.E.W. van Beusekom <rutger.van.beusekom@gmail.com>
;;; Copyright © 2018, 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2019, 2020 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

* iohere-builtin
:script:
#+begin_src sh
  \cat <<EOF
  foobar
  EOF
#+end_src

* iohere
:script:
#+begin_src sh
  cat <<EOF
  foobar
  EOF
#+end_src
:stdout:
#+begin_example
  foobar
#+end_example

* redirect-append
:script:
#+begin_src sh
  echo foo > $TEST_TMP/bar
  echo foo >> $TEST_TMP/bar
  cat $TEST_TMP/bar
  rm $TEST_TMP/bar
#+end_src
:stdout:
#+begin_example
  foo
  foo
#+end_example

* redirect-clobber
:script:
#+begin_src sh
  echo foo > /tmp/bar$$
  cat /tmp/bar$$
  > /tmp/bar$$
  cat /tmp/bar$$
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* redirect-in-out
:script:
#+begin_src sh
  cat < tests/data/foo > /tmp/bar$$
  cat /tmp/bar$$
  rm /tmp/bar$$
#+end_src
:stdout:
#+begin_example
  foo
  bar
  baz
#+end_example

* redirect-in
:script:
#+begin_src sh
  \cat < tests/data/foo
#+end_src

;; * redirect-merge
;; :script:
;; #+begin_src sh
;;   set +e
;;   ls /bin/sh /bin/foo > bar 2>&1
;;   echo foo
;;   cat bar
;;   rm bar
;; #+end_src
;; :stdout:
;; #+begin_example
;;   foo
;;   ls: cannot access '/bin/foo': No such file or directory
;;   /bin/sh
;; #+end_example

* redirect-pipe
:script:
#+begin_src sh
  echo foo | grep foo 2>/dev/null
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* redirect-sed
:script:
#+begin_src sh
  unset DESTDIR
  sed \
      -e "s,^#! /bin/sh,#! /bin/GASH," \
      tests/data/diff.scm > $DESTDIR/tmp/diff.scm
  cat $DESTDIR/tmp/diff.scm
  rm $DESTDIR/tmp/diff.scm
#+end_src
:stdout:
#+begin_example
  #! /bin/GASH
  !#
  
#+end_example

* redirect
:script:
#+begin_src sh
  echo foo 1>/tmp/bar$$
  cat /tmp/bar$$
  rm /tmp/bar$$
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* redirect-space
:script:
#+begin_src sh
  echo foo > /tmp/bar$$
  cat /tmp/bar$$
  rm /tmp/bar$$
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* Files opened for redirect can be executed immediately
:script:
#+begin_src sh
  cat > $TEST_TMP/foo.sh <<EOF
  #!$SYSTEM_SHELL
  echo foo
  EOF
  chmod +x $TEST_TMP/foo.sh
  $TEST_TMP/foo.sh
  rm -f $TEST_TMP/foo.sh
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* Redirecting output respects the noclobber option
:script:
#+begin_src sh
  set -o noclobber
  echo foo > $TEST_TMP/noclobber && echo created
  cat $TEST_TMP/noclobber
  echo bar >| $TEST_TMP/noclobber && echo clobbered
  cat $TEST_TMP/noclobber
  echo baz > $TEST_TMP/noclobber || echo not clobbered
  cat $TEST_TMP/noclobber
  rm -f $TEST_TMP/noclobber
#+end_src
:stdout:
#+begin_example
  created
  foo
  clobbered
  bar
  not clobbered
  bar
#+end_example


# Gash --- Guile As SHell
# Copyright © 2017, 2018, 2019 Timothy Sample <samplet@ngyro.com>
# Copyright © 2018, 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
#
# This file is part of Gash.
#
# Gash is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# Gash is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gash.  If not, see <http://www.gnu.org/licenses/>.

include $(top_srcdir)/build-aux/guile.am

EXTRA_DIST +=                                   \
  .dir-locals.el                                \
  COPYING.CC0                                   \
  THANKS                                        \
  bootstrap                                     \
  build-aux/git-version-gen                     \
  build-aux/gitlab-ci.yml                       \
  build-aux/gitlog-to-changelog                 \
  build-aux/gitlog-to-changelog-repro.patch     \
  guix.scm                                      \
  tests/data/README

# Modules and scripts
#####################

SOURCES =                                       \
  gash/arithmetic.scm                           \
  gash/built-ins/break.scm                      \
  gash/built-ins/cd.scm                         \
  gash/built-ins/colon.scm                      \
  gash/built-ins/command.scm                    \
  gash/built-ins/continue.scm                   \
  gash/built-ins/dot.scm                        \
  gash/built-ins/echo.scm                       \
  gash/built-ins/eval.scm                       \
  gash/built-ins/exec.scm                       \
  gash/built-ins/exit.scm                       \
  gash/built-ins/export.scm                     \
  gash/built-ins/false.scm                      \
  gash/built-ins/pwd.scm                        \
  gash/built-ins/read.scm                       \
  gash/built-ins/readonly.scm                   \
  gash/built-ins/return.scm                     \
  gash/built-ins/set.scm                        \
  gash/built-ins/shift.scm                      \
  gash/built-ins/trap.scm                       \
  gash/built-ins/true.scm                       \
  gash/built-ins/type.scm                       \
  gash/built-ins/umask.scm                      \
  gash/built-ins/unset.scm                      \
  gash/built-ins/utils.scm                      \
  gash/built-ins/wait.scm                       \
  gash/built-ins.scm                            \
  gash/compat/hash-table.scm                    \
  gash/compat/srfi-43.scm                       \
  gash/compat/textual-ports.scm                 \
  gash/compat.scm                               \
  gash/config.scm                               \
  gash/environment.scm                          \
  gash/eval.scm                                 \
  gash/gash.scm                                 \
  gash/lexer.scm                                \
  gash/parser.scm                               \
  gash/pattern.scm                              \
  gash/readline.scm                             \
  gash/repl.scm                                 \
  gash/shell.scm                                \
  gash/word.scm                                 \
  language/sh/spec.scm

bin_SCRIPTS =                                   \
  scripts/gash

do_subst = sed                                  \
  -e 's,[@]GUILE[@],$(GUILE),g'                 \
  -e 's,[@]MODDIR[@],$(moddir),g'               \
  -e 's,[@]GODIR[@],$(ccachedir),g'

scripts/gash: scripts/gash.in Makefile
	mkdir -p scripts
	$(do_subst) < $(srcdir)/scripts/gash.in > scripts/gash
	chmod a+x scripts/gash

EXTRA_DIST += scripts/gash.in

# Documentation
###############

info_TEXINFOS = doc/gash.texi

gash_TEXINFOS = doc/fdl-1.3.texi

EXTRA_DIST +=                                   \
  $(gash_TEXINFOS)                              \
  doc/syntax.txt

# Generate 'version.texi' reproducibly using metadata from Git rather
# than using metadata from the filesystem.
override $(srcdir)/doc/version.texi:
	$(AM_V_GEN)set -e \
	LC_ALL=C; export LC_ALL; \
	TZ=UTC0; export TZ; \
	timestamp=$$(git log --pretty=format:%ct -- doc/gash.texi \
	                 | sort -n -r | head -n 1); \
	dmy=$$(date --date="@$$timestamp" "+%-d %B %Y"); \
	my=$$(date --date="@$$timestamp" "+%B %Y"); \
	{ echo "@set UPDATED $$dmy"; \
	  echo "@set UPDATED-MONTH $$my"; \
	  echo "@set EDITION $(VERSION)"; \
	  echo "@set VERSION $(VERSION)"; } > $@-t; \
	mv $@-t $@

# Tests
#######

AM_TESTS_ENVIRONMENT = SYSTEM_SHELL='$(SHELL)'; export SYSTEM_SHELL;
TEST_EXTENSIONS = .scm .org
SCM_LOG_COMPILER = $(top_builddir)/pre-inst-env $(GUILE)
AM_SCM_LOG_FLAGS = --no-auto-compile
ORG_LOG_COMPILER = $(top_builddir)/pre-inst-env \
                       $(top_builddir)/tests/run-test-suite

.PHONY: check-spec
check-spec:
	$(MAKE) $(AM_MAKEFLAGS) -L -C tests/spec check

.PHONY: check-bootstrap
check-bootstrap:
	guix build -f tests/bootstrap/bash-without-bash.scm

.PHONY: check-gash
check-gash:
	SHELL=gash $(top_builddir)/pre-inst-env $(top_builddir)/check.sh

UNIT_TESTS =                                    \
  tests/unit/eval.scm                           \
  tests/unit/lexer.scm                          \
  tests/unit/parser.scm                         \
  tests/unit/pattern.scm                        \
  tests/unit/shell.scm                          \
  tests/unit/word.scm

FULL_TESTS =                                    \
  tests/arguments.org                           \
  tests/assignments.org                         \
  tests/command-substitution.org                \
  tests/compound-words.org                      \
  tests/conditionals.org                        \
  tests/dot.org                                 \
  tests/eval.org                                \
  tests/exiting.org                             \
  tests/exporting.org                           \
  tests/functions.org                           \
  tests/loops.org                               \
  tests/pipes-and-booleans.org                  \
  tests/read.org                                \
  tests/redirects.org                           \
  tests/signals.org                             \
  tests/temporary-assignments.org               \
  tests/variable-and.org                        \
  tests/variable-operators.org                  \
  tests/variable-or.org                         \
  tests/variable-patterns.org                   \
  tests/variable-words.org                      \
  tests/words.org

TESTS = $(UNIT_TESTS) $(FULL_TESTS)

TEST_DATA_FILES =                               \
  tests/data/star/2                             \
  tests/data/star/3                             \
  tests/data/star/0                             \
  tests/data/star/1                             \
  tests/data/foo                                \
  tests/data/diff.scm                           \
  tests/data/script.sh                          \
  tests/data/42-export-new.sh

.PHONY: test-list
test-list: ; @echo $(TESTS)

EXTRA_DIST +=                                   \
  $(TESTS)                                      \
  $(TEST_DATA_FILES)                            \
  tests/unit/automake.scm                       \
  tests/spec/check-spec                         \
  tests/spec/Makefile.am                        \
  tests/spec/Makefile.in                        \
  tests/spec/oil.scm                            \
  tests/bootstrap/bash-without-bash.scm

# Coverage
##########

if HAVE_GENHTML

lcov.info: all
	$(top_builddir)/pre-inst-env $(top_builddir)/tools/coverage

coverage: lcov.info
	$(GENHTML) -o $(top_builddir)/coverage lcov.info

endif # HAVE_GENHTML

# Distribution
##############

dist-hook: gen-ChangeLog
	echo $(VERSION) > $(distdir)/.tarball-version
	if test -e .git; then \
	    git ls-tree -r --name-only HEAD \
	        > $(distdir)/.tarball-manifest; \
	    git show HEAD --format=%ct --no-patch 2>/dev/null \
	        > $(distdir)/.tarball-timestamp; \
	else \
	    cp $(srcdir)/.tarball-manifest $(distdir)/.tarball-manifest; \
	    cp $(srcdir)/.tarball-timestamp $(distdir)/.tarball-timestamp; \
	fi

build-aux/gitlog-to-changelog-repro: \
		$(srcdir)/build-aux/gitlog-to-changelog \
		$(srcdir)/build-aux/gitlog-to-changelog-repro.patch
	$(AM_V_GEN){ mkdir -p $$(dirname $@) && \
	             patch -t --output=$@ $? && \
	             chmod a+x $@; }

.PHONY: gen-ChangeLog
gen-ChangeLog: build-aux/gitlog-to-changelog-repro
	$(AM_V_GEN)if test -e .git; then \
	    { ./build-aux/gitlog-to-changelog-repro \
	          --format='%s%n%n%b%n' -- v0.1.. && \
	      echo && \
	      sed -n -e '/^Copyright/,$$p' < $(top_srcdir)/ChangeLog; \
	    } > $(distdir)/ChangeLog-t && \
	    { rm -f $(distdir)/ChangeLog && \
	      mv $(distdir)/ChangeLog-t $(distdir)/ChangeLog; } \
	fi

# Reproducible tarball
# Be friendly to Debian; avoid using EPOCH
override GZIP_ENV = "--best --no-name"
am__tar = $${TAR-tar} -chof - --sort=name \
  --mtime=@$$(cat "$$tardir"/.tarball-timestamp) \
  --owner=0 --group=0 --numeric-owner \
  --mode=go=rX,u+rw,a-s "$$tardir"

distcheck-hook:
	set -e;                                 \
	manifest=$(distdir)/.tarball-manifest;  \
	test -f "$$manifest";                   \
	for x in `cat "$$manifest"`;            \
	do                                      \
	    if ! test -f $(distdir)/"$$x"       \
                && ! test "$$x" = .gitignore;   \
	    then                                \
	        echo "Missing: $$x";            \
	        exit 1;                         \
	    fi;                                 \
	done

# Cleaning
##########

CLEANFILES +=                                   \
  $(bin_SCRIPTS)                                \
  $(FULL_TESTS:tests/%.org=tests/%.log)          \
  $(FULL_TESTS:tests/%.org=tests/%.trs)          \
  $(UNIT_TESTS:tests/%.scm=tests/%.log)         \
  $(UNIT_TESTS:tests/%.scm=tests/%.trs)

DISTCLEANFILES = build-aux/gitlog-to-changelog-repro

clean-local:
	$(MAKE) $(AM_MAKEFLAGS) -L -C tests/spec clean

distclean-local:
	$(MAKE) $(AM_MAKEFLAGS) -L -C tests/spec distclean

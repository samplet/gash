;;; Gash -- Guile As SHell
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gash compat srfi-43)
  #:use-module (gash compat))

;;; Commentary:
;;;
;;; The '(srfi srfi-43)' module was introduced into Guile in version
;;; 2.0.10, so we provide a shim.
;;;
;;; Code:

(if-guile-version-below (2 0 10)
  (begin
    (define-public (vector-empty? vec)
      (zero? (vector-length vec)))
    ;; We only need the single vector version.
    (define-public (vector-every pred? vec)
      (let loop ((i 0) (value #t))
        (if (< i (vector-length vec))
            (and value (loop (1+ i) (pred? (vector-ref vec i))))
            value))))
  (begin
    (use-modules (srfi srfi-43))
    (re-export vector-empty? vector-every)))

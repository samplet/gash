;;; Gash -- Guile As SHell
;;; Copyright © 2018 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gash built-ins return)
  #:use-module (gash built-ins utils)
  #:use-module (gash compat)
  #:use-module (gash environment)
  #:use-module (ice-9 match))

;;; Commentary:
;;;
;;; The 'return' utility.
;;;
;;; Code:

(define (main . args)
  (match args
    (() (main (number->string (get-status))))
    ((arg)
     (match (string->exit-status arg)
       (#f (format (current-error-port)
                   "gash: return: argument must be a number from 0 to 255~%")
           (throw 'shell-error))
       (n (sh:return n)
          (format (current-error-port)
                  (string-append "gash: return: no function "
                                 "or sourced script to return from~%"))
          EXIT_SUCCESS)))
    (_ (format (current-error-port)
               "gash: return: too many arguments~%")
       (throw 'shell-error))))

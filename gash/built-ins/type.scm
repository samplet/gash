;;; Gash --- Guile As SHell
;;; Copyright © 2018 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful, but WITHOUT ANY
;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;;; details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gash built-ins type)
  #:use-module (ice-9 getopt-long)
  #:use-module (gash built-ins utils)
  #:use-module (gash compat)
  #:use-module (gash config))

;;; Commentary:
;;;
;;; The 'type' utility.
;;;
;;; Code:

(define (PATH-search-path program)
  (search-path (string-split (getenv "PATH") #\:) program))

(define main
  (case-lambda
    (() #t)
    (args
     (let* ((option-spec
             '((help)
               (canonical-file-name (single-char #\p))
               (version)))
            (options (getopt-long (cons "type" args) option-spec))
            (help? (option-ref options 'help #f))
            (version? (option-ref options 'version #f))
            (files (option-ref options '() '())))
       (cond (help? (display "Usage: type [OPTION]... [COMMAND]

Options:
  --help     display this help and exit
  -p         display canonical file name of COMMAND
  --version  display version information and exit
")
                    EXIT_SUCCESS)
             (version? (format #t "type (GASH) ~a\n" %version) EXIT_SUCCESS)
             ((null? files) EXIT_SUCCESS)
             ((option-ref options 'canonical-file-name #f)
              (let ((command (car files)))
                (if (built-in? command) EXIT_SUCCESS
                    (let ((program (PATH-search-path command)))
                      (cond ((string? program)
                             (format #t "~s\n" program)
                             EXIT_SUCCESS)
                            (else EXIT_FAILURE))))))
             (else
              (let ((command (car files)))
                (cond ((built-in? command)
                       (format #t "~a is a shell builtin\n" command)
                       EXIT_SUCCESS)
                      ((PATH-search-path command) =>
                       (lambda (program)
                         (format #t "~a is ~a\n" command program)
                         EXIT_SUCCESS))
                      (else
                       (format #t "gash: type: ~a: not found\n" command)
                       EXIT_FAILURE)))))))))

;;; Gash -- Guile As SHell
;;; Copyright © 2018 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gash built-ins)
  #:export (search-built-ins
            search-special-built-ins))

;;; Commentary:
;;;
;;; This module provides built-in searching functions.
;;;
;;; Code:

(define (undefined env . args)
  (throw 'undefined-built-in))

;; Special built-ins take precedence over any other command.
(define *special-built-ins*
  `(("." . ,(@@ (gash built-ins dot) main))
    (":" . ,(@@ (gash built-ins colon) main))
    ("break" . ,(@@ (gash built-ins break) main))
    ("continue" . ,(@@ (gash built-ins continue) main))
    ("eval" . ,(@@ (gash built-ins eval) main))
    ("exec" . ,(@@ (gash built-ins exec) main))
    ("exit" . ,(@@ (gash built-ins exit) main))
    ("export" . ,(@@ (gash built-ins export) main))
    ("readonly" . ,(@@ (gash built-ins readonly) main))
    ("return" . ,(@@ (gash built-ins return) main))
    ("set" . ,(@@ (gash built-ins set) main))
    ("shift" . ,(@@ (gash built-ins shift) main))
    ("times" . ,undefined)
    ("trap" . ,(@@ (gash built-ins trap) main))
    ("unset" . ,(@@ (gash built-ins unset) main))))

;; Regular built-ins take precendence over utilities in the search
;; path, but not over functions.
(define *built-ins*
  `( ;; POSIX-specified built-ins.
    ("alias" . ,undefined)
    ("bg" . ,undefined)
    ("cd" . ,(@@ (gash built-ins cd) main))
    ("command" . ,(@@ (gash built-ins command) main))
    ("false" . ,(@@ (gash built-ins false) main))
    ("fc" . ,undefined)
    ("fg" . ,undefined)
    ("getopts" . ,undefined)
    ("hash" . ,undefined)
    ("jobs" . ,undefined)
    ("kill" . ,undefined)
    ("newgrp" . ,undefined)
    ("pwd" . ,(@@ (gash built-ins pwd) main))
    ("read" . ,(@@ (gash built-ins read) main))
    ("true" . ,(@@ (gash built-ins true) main))
    ("type" . ,(@@ (gash built-ins type) main))
    ("umask" . ,(@@ (gash built-ins umask) main))
    ("unalias" . ,undefined)
    ("wait" . ,(@@ (gash built-ins wait) main))
    ;; Other built-ins.
    ("echo" . ,(@@ (gash built-ins echo) main))))

(define (search-special-built-ins name)
  (assoc-ref *special-built-ins* name))

(define (search-built-ins name)
  (assoc-ref *built-ins* name))

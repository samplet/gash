;;; Gash -- Guile As SHell
;;; Copyright © 2017, 2018 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gash repl)
  #:use-module (gash environment)
  #:use-module (gash eval)
  #:use-module (gash parser)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:export (run-repl))

;;; Commentary:
;;;
;;; The read-eval-print loop (REPL) of the shell.
;;;
;;; Code:

(define* (run-repl #:optional (port (current-input-port)) parse?)
  (let loop ((exp (read-sh port)))
    (match exp
      ((? eof-object?) (sh:exit))
      (_ (if parse? (format #t "~a\n" exp)
             (eval-sh exp))
         (reap-child-processes!)
         (loop (read-sh port))))))
